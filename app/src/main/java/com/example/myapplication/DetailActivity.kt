package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.detailpager.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.degrader))

        if(!intent.hasExtra("codeBarre")){
            throw Exception("The product is missing")
        }

        setContentView(R.layout.detailpager)
        setSupportActionBar(toolbar)

        val barcode = intent.getStringExtra("codeBarre")

        GlobalScope.launch {
            val product = NetworkManager.getProduct(barcode)

            withContext(Dispatchers.Main){
                if(product != null){
                    pager.adapter = DetailsAdapter(supportFragmentManager, product)
                    tabs.setupWithViewPager(pager)
                    Log.i("Product_API", product.picture)
                } else{
                    val toast = Toast.makeText(applicationContext, "Error product", Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
        }
    }
}

class DetailsAdapter(fm: FragmentManager, val product: Product) : FragmentPagerAdapter(fm){
    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment {
        return when(position){
            0->FragmentFiche.newInstance(product)
            1->FragmentNutrition.newInstance(product)
            2->FragmentInfosNutrition.newInstance(product)
            else -> throw Exception("Error")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0->"Fiche"
            1->"Nutrition"
            2->"Infos Nutritionnelles"
            else -> throw Exception("Error")
        }
    }
}