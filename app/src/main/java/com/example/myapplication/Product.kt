package com.example.myapplication

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(val additives: Map<String, String>?,
                   val allergens: List<String?>?,
                   val altName: String?,
                   val barcode: String,
                   val brands: List<String?>?,
                   val containsPalmOil: Boolean?,
                   val ingredients: List<String?>?,
                   val manufacturingCountries: List<String?>?,
                   val name: String?,
                   val novaScore: String?,
                   val nutriScore: String?,
                   val picture: String?,
                   val quantity: String?,
                   val traces: List<String?>?): Parcelable{}