package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list.*
import kotlinx.android.synthetic.main.list_item.view.*

class FragmentsList : Fragment(), OnProductClickListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val product = Product(
            mapOf("aa" to "aa"),
            listOf("Aucune"),
            "Cassegrain",
            "5000159484695",
            listOf("azerty"),
            false,
            listOf("aa", "bb", "cc"),
            listOf("aa", "bb", "cc"),
            "Petits Pois",
            "test",
            "A",
            "https://static.openfoodfacts.org/images/products/308/368/008/5304/front_fr.7.400.jpg",
            "200g",
            listOf("Petits pois 66%", "eau", "garniture 2,8% (salade, oignon grelot)", "sucre", "sel", "arôme naturel"))

        val products = listOf(product,product,product,product)
        list.layoutManager = LinearLayoutManager(requireContext()) as RecyclerView.LayoutManager?
        list.adapter = ListAdapter(products, this)
        //change data on view
    }

    override fun onProductClicked(product: Product) {

    }

    override fun onCardClicked(product: Product) {
        val intent = Intent(requireActivity(), DetailActivity::class.java)
        intent.putExtra("codeBarre", product.barcode)
        startActivity(intent)
    }
}

class FragmentsProductFav : Fragment(){

}

class FragmentsStats : Fragment(){

}

class FragmentsProfil : Fragment(){

}

class ListAdapter(val products: List<Product>, val listener: OnProductClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductCell).bindProduct(products[position], listener)
    }

    override fun getItemCount(): Int = products.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductCell(inflater.inflate(R.layout.list_item, parent, false))
    }
}

// Une cellule
class ProductCell(v: View): RecyclerView.ViewHolder(v){

    val cardView : CardView = v.product_item_card

    val productImage : ImageView = v.product_item_image
    val productNom : TextView = v.product_name
    val productMarque : TextView = v.product_brand
    val productBookMark : ImageView = v.product_bookmark
    val productNutriScore : TextView = v.product_nutriscore

    fun bindProduct(product: Product, listener: OnProductClickListener) {

        Picasso.get().load(product.picture).into(productImage)
        productNom.text = product.name
        productMarque.text = product.quantity
        productNutriScore.text = product.nutriScore

        productBookMark.setOnClickListener {
            listener.onProductClicked(product)
        }

        cardView.setOnClickListener {
            listener.onCardClicked(product)
        }

    }
}

interface OnProductClickListener{
    fun onProductClicked(product: Product)
    fun onCardClicked(product: Product)
}